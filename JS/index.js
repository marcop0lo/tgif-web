//Funciones creadas para ocultar y mostrar el acordeón de index.html.
function clickMore() {
    $("#more2").hide();
    $("#less2").show();
};

function clickLess() {
    $("#less2").hide();
    $("#more2").show();
};

$(document).ready(function () {
    $("#more2").click(function () {
        clickMore();
    });
    $("#less2").click(function () {
        clickLess();
    });
});
