const statistics = {
    "numberOfDemocrats": 0,
    "numberOfRepublicans": 0,
    "numberOfIndependents": 0,
    "total": 0,
    "democrats-average-votes-with-party": 0,
    "republicans-average-votes-with-party": 0,
    "independents-average-votes-with-party": 0,
    "total-average": 0,
    "least-engaged": [],
    "most-engaged": [],
    "least-loyal": [],
    "most-loyal": []
};

const totalMembers = {
    "allMembers": null,
    "republican": null,
    "democrat": null,
    "independent": null,

    filterMembers: function () {
        this.allMembers = data.results[0].members;
        this.republican = filterMembersByParty("R");
        this.democrat = filterMembersByParty("D");
        this.independent = filterMembersByParty("I");
    }
}

function filterMembersByParty(str) {
    members = data.results[0].members;
    var filtered = members.filter(function (element) {
        return element.party === str;
    });
    return filtered;
};

function calculateAverageWParty(members) {
    var average = 0;
    average = members.map(m => m.votes_with_party_pct).reduce((pct1, pct2) => pct1 + pct2);
return average = average / members.length;
}

function actStatistics() {
    statistics.numberOfDemocrats = totalMembers.democrat.length;
    statistics.numberOfRepublicans = totalMembers.democrat.length;
    statistics.numberOfIndependents = totalMembers.independent.length;
    statistics.total = totalMembers.allMembers.length;
    statistics["republicans-average-votes-with-party"] = Math.trunc(calculateAverageWParty(totalMembers.republican));
    statistics["democrats-average-votes-with-party"] = Math.trunc(calculateAverageWParty(totalMembers.democrat));
    statistics["independents-average-votes-with-party"] = Math.trunc(calculateAverageWParty(totalMembers.independent));
}

totalMembers.filterMembers();
actStatistics();

console.log(statistics["democrats-average-votes-with-party"] + " " + statistics["independents-average-votes-with-party"] + " " + statistics["republicans-average-votes-with-party"]);

console.log(statistics[]);