//Se crea la variable en la que se guarda el array de miembros (sirve para senate y house).
var membersData = data.results[0].members;

//Funciónn que crea una tabla con cabeceras, recibe un ID de tabla y un array de objetos miembro.
function createRowsTable(tableID, data) {
    var table = document.getElementById(tableID);
    var row = table.insertRow(0);
    row.className = "rowHead";
    row.insertCell(0).innerHTML = "SENATOR NAME";
    row.insertCell(1).innerHTML = "PARTY AFFLICATION";
    row.insertCell(2).innerHTML = "STATE";
    row.insertCell(3).innerHTML = "YEARS IN OFFICE";
    row.insertCell(4).innerHTML = "% OF VOTES W/PARTY"
    data.forEach(function (element) {
        var row = table.insertRow(-1);
        row.className = element.party + ' ' + element.state + ' ' + 'miembro';
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        var cell5 = row.insertCell(4);
        var fullName = element.first_name;
        var isNull = element.middle_name == null;
        isNull ? (fullName += " " + element.last_name) : (fullName += " " + element.middle_name + " " + element.last_name);
        cell1.className = "nameCell";
        cell1.innerHTML = fullName.link(element.url);
        cell2.innerHTML = element.party;
        cell3.innerHTML = element.state;
        cell4.innerHTML = element.seniority;
        cell5.innerHTML = element.votes_with_party_pct + "%";
    })
}

//Llamamos a la función para crear tabla principal al momento de cargar el HTML.
createRowsTable("table-data", membersData);

//Función que recibe un array de objetos, devuelve un listado en array de los valores de la propiedad 'state' de esos objetos en orden Alfabético.
//Nota a futuro: Se puede modificar agregando un parámetro "property" para obtener un array de cualquier propiedad que se desee listar.
function createList(array) {
    var newlist = [];
    array.forEach(function (element) {
        if (!newlist.includes(element.state)) {
            newlist.push(element.state);
        };
    });
    newlist = newlist.sort();
    return newlist;
}

//Crea variable donde se guarda el array para después insertarlo en los Option de States.
var statesList = createList(membersData);

//Función que recibe un ID select y un array para los options, agrega elementos option al Select y al terminar devuelve la cantidad de options de este elemento Select.
function createSelectOptions(selectID, array) {
    var select = document.getElementById(selectID);
    array.forEach(function (element) {
        var option = document.createElement("option");
        option.text = element;
        select.options.add(option, -1);
    })
    return select.options.length;
}

//Llamamos a la función para crear los elementos options y se agreguen en StatesMenu (SELECT)
createSelectOptions("statesMenu", statesList);

//Función que recibe un array de miembros, filtra estos según la opción seleccionada del elemento SELECT y los devuelve en un array.
function filterMembers(data) {
    var filtered = []
    if (($(".form-select option:selected").text()) == "ALL") {
        filtered = data;
    } else {
        data.forEach(function (element) {
            if (element.state == ($(".form-select option:selected").text())) {
                filtered.push(element);
            };
        });
    }
    filtered = filterByCheckBox(filtered);
    return filtered;
}

//Función que recibe el ID de un input y devuelve true si está check o false si no.
function isSelected(inputID) {
    return (document.getElementById(inputID).checked);
}

//Función que muestra los rows de la tabla si un checkbox está checkeado o los oculta si es que no.
function filterByCheckBox(arrayFiltered) {
    var filtered = arrayFiltered;
    if (!isSelected("check-rep")) {
        filtered = filtered.filter(function (element) {
            return element.party != "R";
        })
    }
    if (!isSelected("check-dem")) {
        filtered = filtered.filter(function (element) {
            return element.party != "D";
        })
    }
    if (!isSelected("check-ind")) {
        filtered = filtered.filter(function (element) {
            return element.party != "I";
        })
    }
    return filtered;
}


//Funcion que elimina la tabla actual y crea una nueva en base a los datos del formulario.
function actTable(array) {
    $(".rowHead").remove();
    $(".miembro").remove();
    createRowsTable("table-data", array);
}

var filteredMembers = [];

//JQuery para activar funciones cada vez que se modifican los checkboxes o el select.
$(document).ready(function () {
    $(".form-check").click(function () {
        filteredMembers = filterMembers(membersData);
        actTable(filteredMembers);
    });
    $(".form-select").change(function () {
        filteredMembers = filterMembers(membersData);
        actTable(filteredMembers);
    });
});
